﻿
namespace TMXtoXLSX_Converter
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.MainListBox = new System.Windows.Forms.ListBox();
            this.RemoveButton = new System.Windows.Forms.Button();
            this.TagsGroup = new System.Windows.Forms.GroupBox();
            this.RevertTags = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.optionalExportGroup = new System.Windows.Forms.GroupBox();
            this.EnableChangeDate = new System.Windows.Forms.CheckBox();
            this.EnableCreationDate = new System.Windows.Forms.CheckBox();
            this.EnableDocFileName = new System.Windows.Forms.CheckBox();
            this.EnableContextId = new System.Windows.Forms.CheckBox();
            this.ConvertButton = new System.Windows.Forms.Button();
            this.BrowserGroup = new System.Windows.Forms.GroupBox();
            this.SearchSubFolders = new System.Windows.Forms.CheckBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.labelRemove = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.TagsGroup.SuspendLayout();
            this.optionalExportGroup.SuspendLayout();
            this.BrowserGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainListBox
            // 
            this.MainListBox.FormattingEnabled = true;
            this.MainListBox.HorizontalScrollbar = true;
            this.MainListBox.ItemHeight = 20;
            this.MainListBox.Location = new System.Drawing.Point(14, 95);
            this.MainListBox.Name = "MainListBox";
            this.MainListBox.ScrollAlwaysVisible = true;
            this.MainListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.MainListBox.Size = new System.Drawing.Size(787, 284);
            this.MainListBox.Sorted = true;
            this.MainListBox.TabIndex = 0;
            // 
            // RemoveButton
            // 
            this.RemoveButton.Location = new System.Drawing.Point(670, 390);
            this.RemoveButton.Name = "RemoveButton";
            this.RemoveButton.Size = new System.Drawing.Size(109, 33);
            this.RemoveButton.TabIndex = 4;
            this.RemoveButton.Text = "Remove";
            this.RemoveButton.UseVisualStyleBackColor = true;
            this.RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
            // 
            // TagsGroup
            // 
            this.TagsGroup.Controls.Add(this.RevertTags);
            this.TagsGroup.Controls.Add(this.label1);
            this.TagsGroup.Location = new System.Drawing.Point(11, 422);
            this.TagsGroup.Name = "TagsGroup";
            this.TagsGroup.Size = new System.Drawing.Size(790, 100);
            this.TagsGroup.TabIndex = 5;
            this.TagsGroup.TabStop = false;
            this.TagsGroup.Text = "Tags";
            // 
            // RevertTags
            // 
            this.RevertTags.AutoSize = true;
            this.RevertTags.Checked = true;
            this.RevertTags.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RevertTags.Location = new System.Drawing.Point(15, 44);
            this.RevertTags.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RevertTags.Name = "RevertTags";
            this.RevertTags.Size = new System.Drawing.Size(329, 24);
            this.RevertTags.TabIndex = 3;
            this.RevertTags.Text = "Revert Tags to their original pre-tagged state";
            this.RevertTags.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 2;
            // 
            // optionalExportGroup
            // 
            this.optionalExportGroup.Controls.Add(this.EnableChangeDate);
            this.optionalExportGroup.Controls.Add(this.EnableCreationDate);
            this.optionalExportGroup.Controls.Add(this.EnableDocFileName);
            this.optionalExportGroup.Controls.Add(this.EnableContextId);
            this.optionalExportGroup.Location = new System.Drawing.Point(823, 9);
            this.optionalExportGroup.Name = "optionalExportGroup";
            this.optionalExportGroup.Size = new System.Drawing.Size(239, 296);
            this.optionalExportGroup.TabIndex = 6;
            this.optionalExportGroup.TabStop = false;
            this.optionalExportGroup.Text = "Aditional Information to Export";
            // 
            // EnableChangeDate
            // 
            this.EnableChangeDate.AutoSize = true;
            this.EnableChangeDate.Checked = true;
            this.EnableChangeDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableChangeDate.Location = new System.Drawing.Point(27, 200);
            this.EnableChangeDate.Name = "EnableChangeDate";
            this.EnableChangeDate.Size = new System.Drawing.Size(117, 24);
            this.EnableChangeDate.TabIndex = 3;
            this.EnableChangeDate.Text = "Change Date";
            this.EnableChangeDate.UseVisualStyleBackColor = true;
            // 
            // EnableCreationDate
            // 
            this.EnableCreationDate.AutoSize = true;
            this.EnableCreationDate.Checked = true;
            this.EnableCreationDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableCreationDate.Location = new System.Drawing.Point(27, 152);
            this.EnableCreationDate.Name = "EnableCreationDate";
            this.EnableCreationDate.Size = new System.Drawing.Size(123, 24);
            this.EnableCreationDate.TabIndex = 2;
            this.EnableCreationDate.Text = "Creation Date";
            this.EnableCreationDate.UseVisualStyleBackColor = true;
            // 
            // EnableDocFileName
            // 
            this.EnableDocFileName.AutoSize = true;
            this.EnableDocFileName.Checked = true;
            this.EnableDocFileName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableDocFileName.Location = new System.Drawing.Point(27, 48);
            this.EnableDocFileName.Name = "EnableDocFileName";
            this.EnableDocFileName.Size = new System.Drawing.Size(164, 24);
            this.EnableDocFileName.TabIndex = 1;
            this.EnableDocFileName.Text = "Document Filename";
            this.EnableDocFileName.UseVisualStyleBackColor = true;
            // 
            // EnableContextId
            // 
            this.EnableContextId.AutoSize = true;
            this.EnableContextId.Checked = true;
            this.EnableContextId.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableContextId.Location = new System.Drawing.Point(27, 100);
            this.EnableContextId.Name = "EnableContextId";
            this.EnableContextId.Size = new System.Drawing.Size(101, 24);
            this.EnableContextId.TabIndex = 0;
            this.EnableContextId.Text = "Context ID";
            this.EnableContextId.UseVisualStyleBackColor = true;
            // 
            // ConvertButton
            // 
            this.ConvertButton.Location = new System.Drawing.Point(823, 433);
            this.ConvertButton.Name = "ConvertButton";
            this.ConvertButton.Size = new System.Drawing.Size(239, 89);
            this.ConvertButton.TabIndex = 7;
            this.ConvertButton.Text = "Convert";
            this.ConvertButton.UseVisualStyleBackColor = true;
            this.ConvertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // BrowserGroup
            // 
            this.BrowserGroup.Controls.Add(this.SearchSubFolders);
            this.BrowserGroup.Controls.Add(this.BrowseButton);
            this.BrowserGroup.Location = new System.Drawing.Point(11, 9);
            this.BrowserGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BrowserGroup.Name = "BrowserGroup";
            this.BrowserGroup.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BrowserGroup.Size = new System.Drawing.Size(790, 72);
            this.BrowserGroup.TabIndex = 8;
            this.BrowserGroup.TabStop = false;
            this.BrowserGroup.Text = "Browse for Folders to Use";
            // 
            // SearchSubFolders
            // 
            this.SearchSubFolders.AutoSize = true;
            this.SearchSubFolders.Location = new System.Drawing.Point(167, 30);
            this.SearchSubFolders.Name = "SearchSubFolders";
            this.SearchSubFolders.Size = new System.Drawing.Size(224, 24);
            this.SearchSubFolders.TabIndex = 5;
            this.SearchSubFolders.Text = "Include Sub-Folders in search";
            this.SearchSubFolders.UseVisualStyleBackColor = true;
            this.SearchSubFolders.CheckedChanged += new System.EventHandler(this.SearchSubFolders_CheckedChanged);
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(15, 25);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(113, 32);
            this.BrowseButton.TabIndex = 4;
            this.BrowseButton.Text = "...";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // labelRemove
            // 
            this.labelRemove.AutoSize = true;
            this.labelRemove.Location = new System.Drawing.Point(17, 396);
            this.labelRemove.Name = "labelRemove";
            this.labelRemove.Size = new System.Drawing.Size(414, 20);
            this.labelRemove.TabIndex = 9;
            this.labelRemove.Text = "To remove multiple files, Shift+Select them and click Remove";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(14, 540);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1048, 33);
            this.progressBar.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 585);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.labelRemove);
            this.Controls.Add(this.BrowserGroup);
            this.Controls.Add(this.ConvertButton);
            this.Controls.Add(this.optionalExportGroup);
            this.Controls.Add(this.TagsGroup);
            this.Controls.Add(this.RemoveButton);
            this.Controls.Add(this.MainListBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "TMX to XLSX";
            this.TagsGroup.ResumeLayout(false);
            this.TagsGroup.PerformLayout();
            this.optionalExportGroup.ResumeLayout(false);
            this.optionalExportGroup.PerformLayout();
            this.BrowserGroup.ResumeLayout(false);
            this.BrowserGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox MainListBox;
        private System.Windows.Forms.Button RemoveButton;
        private System.Windows.Forms.GroupBox TagsGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox optionalExportGroup;
        private System.Windows.Forms.CheckBox EnableChangeDate;
        private System.Windows.Forms.CheckBox EnableCreationDate;
        private System.Windows.Forms.CheckBox EnableDocFileName;
        private System.Windows.Forms.CheckBox EnableContextId;
        private System.Windows.Forms.Button ConvertButton;
        private System.Windows.Forms.CheckBox RevertTags;
        private System.Windows.Forms.GroupBox BrowserGroup;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.Label labelRemove;
        private System.Windows.Forms.CheckBox SearchSubFolders;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}

