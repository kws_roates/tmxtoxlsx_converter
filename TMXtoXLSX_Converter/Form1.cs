﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace TMXtoXLSX_Converter
{
    public partial class Form1 : Form
    {
        BackgroundWorker backgroundWorker_oWorker;
        public Form1()
        {
            InitializeComponent();
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            
            backgroundWorker_oWorker = new BackgroundWorker();
            backgroundWorker_oWorker.DoWork += new DoWorkEventHandler(BackgroundWorker_oWorker_DoWork);
            backgroundWorker_oWorker.ProgressChanged += new ProgressChangedEventHandler(BackgroundWorker_oWorker_ProgressChanged);
            backgroundWorker_oWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundWorker_oWorker_Completed);
            backgroundWorker_oWorker.WorkerReportsProgress = true;
            backgroundWorker_oWorker.WorkerSupportsCancellation = false;
            
        }

        //threaded worker logic goes here

        private void BackgroundWorker_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int count = 0;
            int maxcout = MainListBox.Items.Count;

            foreach (var file in MainListBox.Items)
            {
                Debug.WriteLine($"Converting {file}");
                ConvertTMXtoXLSX(file.ToString());
                count++;
                backgroundWorker_oWorker.ReportProgress(Percentage(count, maxcout));
            }
        }

        private void BackgroundWorker_oWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void BackgroundWorker_oWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (MainListBox.Items.Count > 1)
            {
                MessageBox.Show($"{MainListBox.Items.Count} Files Converted");
            }
            else
            {
                MessageBox.Show($"{MainListBox.Items.Count} File Converted");
            }
            BrowserGroup.Enabled = true;
            MainListBox.Enabled = true;
            labelRemove.Enabled = true;
            RemoveButton.Enabled = true;
            TagsGroup.Enabled = true;
            optionalExportGroup.Enabled = true;
            ConvertButton.Enabled = true;
            progressBar.Value = 0;
            MainListBox.Items.Clear();
        }

        //thread utils

        private void ConvertTMXtoXLSX(string filename)
        {


            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);
            XmlNodeList tus = xmlDoc.GetElementsByTagName("tu");

            ExcelPackage wb = new ExcelPackage();
            ExcelWorksheet ws = wb.Workbook.Worksheets.Add("Sheet1");

            int rowx = 1;
            int seg = 0;

            string mq_time_pattern = @"^(\d{4})(\d{2})(\d{2})(T)(\d{2})(\d{2})(\d{2})(Z)";
            string tm_time_pattern = "$1/$2/$3 $5:$6:$7 UTC";

            int header_col = 0;

            int Doc_col = -1;
            int Context_col = -1;
            int Creationdate_col = -1;
            int Changedate_col = -1;
            int Source_col = -1;
            int Target_col = -1;

            if (EnableDocFileName.Checked == true)
            {
                header_col++;
                Doc_col = header_col;
            }
            if (EnableContextId.Checked == true)
            {
                header_col++;
                Context_col = header_col;
            }
            if (EnableCreationDate.Checked == true)
            {
                header_col++;
                Creationdate_col = header_col;
            }
            if (EnableChangeDate.Checked == true)
            {
                header_col++;
                Changedate_col = header_col;
            }
            if(Source_col == -1)
            {
                header_col++;
                Source_col = header_col;
            }
            if(Target_col == -1)
            {
                header_col++;
                Target_col = header_col;
            }

            Debug.WriteLine($"doc:{Doc_col} context:{Context_col} creation:{Creationdate_col} changed:{Changedate_col} source:{Source_col} target:{Target_col}");

            if (Doc_col != -1)
            {
                ws.Cells[rowx, Doc_col].Value = "Document";
            }
            if (Context_col != -1)
            {
                ws.Cells[rowx, Context_col].Value = "Context";
            }
            if (Creationdate_col != -1)
            {
                ws.Cells[rowx, Creationdate_col].Value = "Creationdate";
            }
            if (Changedate_col != -1)
            {
                ws.Cells[rowx, Changedate_col].Value = "Changedate";
            }
            
            ws.Cells[rowx, Source_col].Value = "Source";
            ws.Cells[rowx, Target_col].Value = "Target";

            rowx++;
            Console.WriteLine($"Enteries: {tus.Count}");

            for (int i = 0; i < tus.Count; i++)
            {

                //Console.WriteLine(tus[i].Attributes.Count);
                for (int att = 0; att < tus[i].Attributes.Count; att++)
                {

                    if (tus[i].Attributes[att].Name == "creationdate")
                    {
                        //Console.WriteLine(tus[i].Attributes[att].Name);
                        //Console.WriteLine(tus[i].Attributes[att].Value);
                        string timex = Regex.Replace(tus[i].Attributes[att].Value, mq_time_pattern, tm_time_pattern);
                        if (Creationdate_col != -1)
                        {
                            ws.Cells[rowx, Creationdate_col].Value = timex;
                        }
                        
                    }
                    if (tus[i].Attributes[att].Name == "changedate")
                    {
                        //Console.WriteLine(tus[i].Attributes[att].Name);
                        //Console.WriteLine(tus[i].Attributes[att].Value);
                        string timex = Regex.Replace(tus[i].Attributes[att].Value, mq_time_pattern, tm_time_pattern);
                        if (Changedate_col != -1)
                        {
                            ws.Cells[rowx, Changedate_col].Value = timex;
                        }
                        
                    }


                }

                for (int j = 0; j < tus[i].ChildNodes.Count; j++)
                {

                    for (int att_childern = 0; att_childern < tus[i].ChildNodes[j].Attributes.Count; att_childern++)
                    {

                        //Console.WriteLine(tus[i].ChildNodes[j].Attributes[att_childern].Value);
                        if (tus[i].ChildNodes[j].Attributes[att_childern].Value == "x-document")
                        {

                            string filex = new FileInfo(tus[i].ChildNodes[j].InnerXml).Name;
                            //Console.WriteLine(filex);
                            if (Doc_col != -1)
                            {
                                ws.Cells[rowx, Doc_col].Value = filex;
                            }
                            
                        }
                        if (tus[i].ChildNodes[j].Attributes[att_childern].Value == "x-context")
                        {
                            //Console.WriteLine(tus[i].Attributes[att].Name);
                            //Console.WriteLine(tus[i].ChildNodes[j].InnerXml);
                            if (Context_col != -1)
                            {
                                ws.Cells[rowx, Context_col].Value = tus[i].ChildNodes[j].InnerXml;
                            }
                            
                        }




                        if (tus[i].ChildNodes[j].Attributes[att_childern].LocalName == "lang")
                        {


                            for (int childern_lang = 0; childern_lang < tus[i].ChildNodes[j].ChildNodes.Count; childern_lang++)
                            {
                                //Console.WriteLine(tus[i].ChildNodes[j].ChildNodes[childern_lang].Name);
                                if (tus[i].ChildNodes[j].ChildNodes[childern_lang].Name == "seg")
                                {
                                    //Console.WriteLine($"{CleanAndUnlock(tus[i].ChildNodes[j].ChildNodes[childern_lang].InnerXml)}");
                                    if (IsOdd(seg) == false)
                                    {
                                        //even segments get put into the source
                                        if (RevertTags.Checked==true)
                                        {
                                            ws.Cells[rowx, Source_col].Value = CleanAndUnlock(tus[i].ChildNodes[j].ChildNodes[childern_lang].InnerXml);
                                        }
                                        else
                                        {
                                            ws.Cells[rowx, Source_col].Value =tus[i].ChildNodes[j].ChildNodes[childern_lang].InnerXml;
                                        }
                                        
                                    }
                                    else
                                    {
                                        if (RevertTags.Checked == true)
                                        {
                                            ws.Cells[rowx, Target_col].Value = CleanAndUnlock(tus[i].ChildNodes[j].ChildNodes[childern_lang].InnerXml);
                                        }
                                        else
                                        {
                                            ws.Cells[rowx, Target_col].Value = tus[i].ChildNodes[j].ChildNodes[childern_lang].InnerXml;
                                        }
                                            
                                    }
                                    seg++;
                                }
                                /* If the tm didn't have a context it uses text flow ... we don't need to include that here 

                                for (int children_lang_attr = 0; children_lang_attr < tus[i].ChildNodes[j].ChildNodes[childern_lang].Attributes.Count; children_lang_attr++)
                                {
                                    Console.WriteLine($"{childern_lang} - {tus[i].ChildNodes[j].ChildNodes[childern_lang].Name} - {tus[i].ChildNodes[j].ChildNodes[childern_lang].Attributes[children_lang_attr].Name} - {tus[i].ChildNodes[j].ChildNodes[childern_lang].Attributes[children_lang_attr].Value}");
                                }
                                */
                            }

                        }

                    }

                }
                //Console.WriteLine("-----------------------------------------");
                rowx++;

            }

            


            if (tus.Count > 0)
            {
                wb.SaveAs(new FileInfo(filename.Replace(".tmx", ".xlsx")));
            }

        }



        static string CleanAndUnlock(string texttoclean)
        {
            string cleaned_string = Regex.Replace(texttoclean, @"<\/?seg>", "");
            cleaned_string = Regex.Replace(cleaned_string, "<ph>.*?displaytext=\"(.*?)\".*?</ph>", "$1");
            //more cleaning
            cleaned_string = Regex.Replace(cleaned_string, "<it pos=\"begin\">.*?displaytext=\"(.*?)\".*?</it>", "$1");

            //added 2020-12-09
            cleaned_string = Regex.Replace(cleaned_string, "<bpt .*?</bpt>", "");
            cleaned_string = Regex.Replace(cleaned_string, "<ept .*?</ept>", "");
            cleaned_string = Regex.Replace(cleaned_string, "<it .*?</it>", "");

            //change 2021-06-07
            //cleaned_string = Regex.Replace(cleaned_string, "<ph>&lt;mq:ch.*?val=\"(\r?\n)\".*?</ph>", "\\n");
            cleaned_string = Regex.Replace(cleaned_string, "<ph>&lt;mq:ch.*?val=\"(\r?\n)\".*?</ph>", "\n");

            cleaned_string = Regex.Replace(cleaned_string, "<ph>&lt;mq:ch.*?val=\"(.*?)\".*?</ph>", "$1");
            cleaned_string = cleaned_string.Replace("&amp;", "&");
            cleaned_string = cleaned_string.Replace("&quot;", "\"");
            cleaned_string = cleaned_string.Replace("&lt;", "<");
            cleaned_string = cleaned_string.Replace("&gt;", ">");
            //more cleaning 
            cleaned_string = Regex.Replace(cleaned_string, "<ph type=\".*?</ph>", " ");
            cleaned_string = Regex.Replace(cleaned_string, "<ph><br /></ph>", "<br />");
            cleaned_string = Regex.Replace(cleaned_string, "<ph.*?/ph>", " ");

            //This may need to be optional
            cleaned_string = cleaned_string.Replace("\\n", "\n");
            //cleaned_string = Regex.Replace(cleaned_string, "&#xA;", "\n");
            return cleaned_string.Trim();
        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        private int Percentage(int currentcount, int maxcount)
        {
            int percentage = (currentcount) * 100 / maxcount;
            return percentage;
        }


        // UI event based logic goes here
        private void BrowseButton_Click(object sender, EventArgs e)
        {
            //bool fileOpened = false;
            using (FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog())
            {
                // Show the FolderBrowserDialog.
                DialogResult result = folderBrowserDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //DirectoryPath.Text = folderBrowserDialog1.SelectedPath;
                    SearchOption searchOption = SearchOption.TopDirectoryOnly;

                    if (SearchSubFolders.Checked == true)
                    {
                        searchOption = SearchOption.AllDirectories;
                    }

                    foreach (string tmx in Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.tmx", searchOption))
                    {
                        //var inlist = MainListBox.Items;
                        if (!MainListBox.Items.Contains(tmx))
                        {
                            MainListBox.Items.Add(tmx);
                        }
                    }

                }
            }

        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {

            while (MainListBox.SelectedIndices.Count > 0)
            {
                for (int i = 0; i < MainListBox.SelectedIndices.Count; i++)
                {

                    Debug.WriteLine(MainListBox.Items[MainListBox.SelectedIndices[i]].ToString());
                    MainListBox.Items.RemoveAt(MainListBox.SelectedIndices[i]);
                }
            }

        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
            BrowserGroup.Enabled = false;
            MainListBox.Enabled = false;
            labelRemove.Enabled = false;
            RemoveButton.Enabled = false;
            TagsGroup.Enabled = false;
            optionalExportGroup.Enabled = false;
            ConvertButton.Enabled = false;

            backgroundWorker_oWorker.RunWorkerAsync();
        }

        private void DirectoryPath_TextChanged(object sender, EventArgs e)
        {

        }

        private void SearchSubFolders_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
